#programa calc

#funcion de suma
def add(a, b):
    result = a + b
    return result

#funciond de resta
def sub(a, b):
    result = a - b
    return result

#suma 1 y 2
print("Suma 1 + 2 = ", add(1,2))

#suma 3 y 4
print("Suma 3 + 4 = ", add(3, 4))

#resta 5 de 6
print("Resta 5 de 6 = ", sub(5, 6))

#resta 7 de 8
print("Resta 7 de 8 = ", sub(7, 8))
